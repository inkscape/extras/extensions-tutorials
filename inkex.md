# inkex module API

This is the top-level `inkex` API without deprecated symbols and filtered for namespace pollution (e.g. doesn't list `inkex.math`).
```
inkex.Color(color=None, space='rgb')
    An RGB array for the color
```
```
inkex.Group
    Any group element (layer or regular group)
```
```
inkex.PathElement
    Provide a useful extension for path elements
```
```
inkex.ShapeElement
    Elements which have a visible representation on the canvas
```
```
inkex.CallExtension(*args, **kwargs)
    Call an external program to get the output
```
```
inkex.EffectExtension()

    Takes the SVG from Inkscape, modifies the selection or the document
    and returns an SVG to Inkscape.

```
```
inkex.GenerateExtension()

    Does not need any SVG, but instead just outputs an SVG fragment which is
    inserted into Inkscape, centered on the selection.

```
```
inkex.InputExtension()

    Takes any type of file as input and outputs SVG which Inkscape can read.

    Used in functions for `Open`

```
```
inkex.OutputExtension()

    Takes the SVG from Inkscape and outputs it to something that's not an SVG.

    Used in functions for `Save As`

```
```
inkex.CubicSuperPath(items)

    A conversion of a path into a predictable list of cubic curves which
    can be operated on as a list of simplified instructions.

    When converting back into a path, all lines, arcs etc will be coverted
    to curve instructions.

    Structure is held as [SubPath[(point_a, bezier, point_b), ...]], ...]

```
```
inkex.Path(path_d=None)
    A list of segment commands which combine to draw a shape
```
```
inkex.Style(style=None, callback=None, **kw)
    A list of style directives
```
```
inkex.BoundingBox(x=None, y=None)

    Some functions to compute a rough bbox of a given list of objects.

    BoundingBox() - Empty bounding box, bool == False
    BoundingBox(x)
    BoundingBox(x, y)
    BoundingBox((x1, x2, y1, y2))
    BoundingBox((x1, x2), (y1, y2))
    BoundingBox(((x1, y1), (x2, y2)))

```
```
inkex.Scale(value=None, *others)
    A pair of numbers that represent the minimum and maximum values.
```
```
inkex.Segment(x=None, y=None)

    A segment and a bounding box are functionally the same, except that
    the coords mean something slightly different.

    Segment(x1, x2, y1, y2)
    Segment(((x1, y1), (x2, y2)))

```
```
inkex.Transform(matrix=None, callback=None, **extra)
    A transformation object which will always reduce to a matrix and can
        then be used in combination with other transformations for reducing
        finding a point and printing svg ready output.

        Use with svg transform attribute input:

          tr = Transform("scale(45, 32)")

        Use with triad matrix input (internal representation):

          tr = Transform(((1.0, 0.0, 0.0), (0.0, 1.0, 0.0)))

        Use with hexad matrix input (i.e. svg matrix(...)):

          tr = Transform((1.0, 0.0, 0.0, 1.0, 0.0, 0.0))

        Once you have a transformation you can operate tr * tr to compose,
        any of the above inputs are also valid operators for composing.

```
```
inkex.AbortExtension(message='')
    Raised to print a message to the user without backtrace
```
```
inkex.addNS(tag, ns=None)
    Add a known namespace to a name for use with lxml
```
```
inkex.errormsg(msg)
    Intended for end-user-visible error messages.

           (Currently just writes to stderr with an appended newline, but could do
           something better in future: e.g. could add markup to distinguish error
           messages from status messages or debugging output.)

           Note that this should always be combined with translation:

             import inkex
             ...
             inkex.errormsg(_("This extension requires two selected paths."))

```
```
inkex.inkbool(value)
    Turn a boolean string into a python boolean
```
## Script which generated the previous section

```import inkex
import textwrap
import types
import inspect

output = open('api.md', 'w')

def get_mod(name):
    try:
        return getattr(inkex, name).__module__
    except AttributeError:
        return '?'

for name in sorted(dir(inkex), key=get_mod):
    mod = get_mod(name)

    # skip private and builtin names
    if name.startswith('_'):
        continue

    value = getattr(inkex, name)

    # skip modules
    if isinstance(value, types.ModuleType):
        continue

    modname = getattr(value, '__module__', '')

    if modname:
        # skip non-inkex items
        if not modname.startswith('inkex'):
            continue
        # skip deprecated API
        if modname == 'inkex.deprecated':
            continue
        # skip private modules
        if modname.startswith('_'):
            continue

    # callable signature
    try:
        sig = inspect.signature(value)
    except:
        sig = ''

    output.write('```\n')
    output.write(f'inkex.{name}{sig}\n')
    print(f"{mod}.{name}{sig}")
    try:
        # don't print the type doc
        if value.__doc__ == type(value).__doc__:
            continue
        output.write(textwrap.indent(textwrap.dedent(value.__doc__), "    "))
        output.write("\n")
    except:
        pass
    finally:
        output.write('```\n')

output.close()
```