(note: to explain the structures provided by the inkex/base.py framework)

# Resources

* [Tutorial Video](no-link)
* [Template Files](no-link)
* [Example Code](no-link)
* [Example SVG](no-link)

# Introduction

We are going to write an extension wich follows the previous tutorial but extending it to provide the user with some options about the colours to set and if we should ignore unclosed paths.

Open your previous example or download and install the prepared template if you skipped the basic extension tutorial.

# Step One

Firstly we need to modify the inx file to include the options that we want to have available in Inkscape. Open it in a text editor and place the following xml under the `id` tag. You can use a program such as xmllint if you want to make sure you've formatted the xml correctly before trying it in Inkscape.

```
[INX PARAMS] TODO
```

# Step Two

We're going to use a mix of parameter types to give you some familiarity with the different options available. Once saved, we can ask Inkscape to run our extension and we now see Inkscape asks us for the options specified. The problem is that our python code is not expecting any additional options and causes an error. So we must tell python that we are going to have some options given to us. Because we are using the inkex module, we only need to add a method called `add_arguments` which handles all our options. Add this code snippet to your python code above the effect method added in the first tutorial.

```
[ARGUMENTS CODE] TODO
```

# Step Three

Each of the arguments is matched with the param in the inx file we added above. The types are often similar, but not always the same, so be aware of what type to use. See the testing and code quality tutorial for how to automatically check the inx and python options are the same.

If we run the extension now we see the code no longer causes an error, but at the same time the options don't do anything. So we need to modify our code to use the arguments passed in. Replace the effect method with this code:

```
[UPDATED CODE] TODO
```

Tutorial Unfinished....